﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1Var2
{
    enum Frequency
    {
        Weekly,
        Monthly,
        Yearly
    }

    class Magazine
    {
        private string name;
        private Frequency periodicity;
        private DateTime dateOfRelease;
        private int circulation;
        private Article[] articles;

        public string GetSetName
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }   

        public Frequency GetSetPeriodicity
        {
            get
            {
                return periodicity;
            }

            set
            {
                periodicity = value;
            }
        }      

        public DateTime GetSetDateOfRelease
        {
            get
            {
                return dateOfRelease;
            }

            set
            {
                dateOfRelease = value;
            }
        }       

        public int getSetCirculation
        {
            get
            {
                return circulation;
            }

            set
            {
                circulation = value;
            }
        }

        public Article[] getSetArticles
        {
            get
            {
                return articles;
            }

            set
            {
                articles = value;
            }
        }

        public Magazine(string newName, Frequency newPeriodicity, DateTime newDateOfRelease, int newCirculation)
        {
            name = newName;
            periodicity = newPeriodicity;
            dateOfRelease = newDateOfRelease;
            circulation = newCirculation;
        }

        public Magazine()
        {
            name = null;
            periodicity = Frequency.Monthly;
            dateOfRelease = DateTime.Today;
            circulation = 0;
            articles = null;
        }

        public double GetAverageRating
        {
            get
            {
                if (articles == null)
                    return 0;
                int i = 0;
                double sum = 0;
                while (articles[i] != null)
                {
                    sum += articles[i].rating;
                }
                return sum / articles.Length;
            }
        }

        public bool this[Frequency index]
        {
            get
            {
                return periodicity == index;
            }
        }

        public void AddArticles(params Article[] newArticles)
        {
            articles = new Article[newArticles.Length];
                articles = newArticles;
        }

        public void AddArticle(Article newArticle)
        {
            Array.Resize(ref articles, articles.Length + 1);
            articles[articles.Length - 1] = newArticle;
        }

        public override string ToString()
        {
            string str="";
            for (int i = 0; i < articles.Length; ++i)
                str += "\n" + (i+1) + ": " + articles[i] + ";";
            return "Magazine {" +
                "name = '" + name + '\'' +
                ", periodicity = '" + periodicity + '\'' +
                ", dateOfRelease = " + dateOfRelease +
                ", circulation = " + circulation +
                ", articles = " + str +
                '}';
        }

        public virtual string ToShortString()
        {
            return "Magazine {" +
                "name = '" + name + '\'' +
                ", periodicity = '" + periodicity + '\'' +
                ", dateOfRelease = " + dateOfRelease +
                ", circulation = " + circulation +
                ", averageRating = " + GetAverageRating +
                '}';
        }
    }
}
