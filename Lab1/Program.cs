﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace lab1Var2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Person Vasia = new Person ("Vasia", "Plantus", new DateTime(1990,9,16));
            Article A = new Article (Vasia,"Job", 45), B = new Article(Vasia, "Lob", 15), C = new Article(Vasia, "Rob", 5);
            Article[] news = {A,B,C};
            Magazine Max = new Magazine("Max", Frequency.Weekly, new DateTime(2018,2,19), 2000);
            Max.AddArticles(news);
            Max.AddArticle(A);
            Console.WriteLine(Max[Frequency.Weekly]);
            Console.WriteLine(Max);
            Console.ReadKey();
            /*
            Person Vasia = new Person("Vasia", "Plantus", new DateTime(1990, 9, 16));
            Article firstArticle = new Article(Vasia, "Job", 45);
            Console.WriteLine("Timer in massifs:\n\nEnter the size of array splited by 1 space: ");

            string userInput = Console.ReadLine();
            int nRows = Int32.Parse(userInput.Split(' ')[0]);
            int mColumns = Int32.Parse(userInput.Split(' ')[1]);

            Console.WriteLine("N = " + nRows);
            Console.WriteLine("M = " + mColumns);
            int sum = 0;
            int size = 0;
            while (sum < nRows * mColumns)
            {
                sum += ++size;
            }
            Console.WriteLine("Sum = "+sum);
            Console.WriteLine("Size = " + size);
            Article[] oneDimension = new Article[nRows * mColumns];
            Article[,] twoDimension = new Article[nRows, mColumns];
            Article[][] jagged = new Article[size][];

            var timeStart = Environment.TickCount;
            for (int i = 0; i < nRows * mColumns; i++)
            {
                oneDimension[i] = firstArticle;
            }
            var timeEnd = Environment.TickCount;
            Console.WriteLine("\nOne dimension is: " + (timeEnd - timeStart));
            
            for (int i = 0; i < nRows * mColumns; i++)
            {
                Console.WriteLine(oneDimension[i]);
            }

            timeStart = Environment.TickCount;
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < mColumns; j++)
                {
                    twoDimension[i, j] = firstArticle;
                }
            }
            timeEnd = Environment.TickCount;
            Console.WriteLine("\nTwo dimension is: " + (timeEnd - timeStart));
            
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < mColumns; j++)
                {
                    Console.WriteLine(twoDimension[i, j]);
                }
            }
            for (int i = 0; i < size; i++)
            {
                if (i == size - 1)
                {
                    jagged[i] = new Article[nRows * mColumns - (sum - size)];
                    break;
                }
                jagged[i] = new Article[i + 1];
            }

            timeStart = Environment.TickCount;
            foreach (var lineArray in jagged)
            {
                for (var j = 0; j < lineArray.Length; j++)
                {
                    lineArray[j] = firstArticle;
                }
            }
            timeEnd = Environment.TickCount;
            Console.WriteLine("\nJagged array is: " + (timeEnd - timeStart)); 
             
             for (int i = 0; i < jagged.Length; i++)
             {
                 Console.Write("\nElement({0}): ", i);
                 for (int j = 0; j < jagged[i].Length; j++)
                 {
                     Console.Write("{0}{1}", jagged[i][j], j == (jagged[i].Length - 1) ? "" : " ");
                 }
                 Console.WriteLine();
             }*/
             Console.ReadKey();
        }
    }
}
