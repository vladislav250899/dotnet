﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1Var2
{
    class Article
    {
        public Person author;
        public string name;
        public double rating;

        public Article(Person newAuthor, string newName, double newRating)
        {
            author = newAuthor;
            name = newName;
            rating = newRating;
        }

        public Article()
        {
            author = null;
            name = null;
            rating = 0;
        }

        public override string ToString()
        {
            return "Article {" +
                "author = " + author +
                ", name = '" + name + '\'' +
                ", rating = " + rating +
                '}';
        }
    }
}
