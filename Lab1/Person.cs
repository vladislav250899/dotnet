﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1Var2
{
    class Person
    {
        private string name;
        private string surname;
        private DateTime dateOfBirth;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }

            set
            {
                surname = value;
            }
        }

        public DateTime DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }

            set
            {
                dateOfBirth = value;
            }
        }

        public int YearOfBirth
        {
            get
            {
                return dateOfBirth.Year;
            }

            set
            {
                dateOfBirth = new DateTime(value, dateOfBirth.Month, dateOfBirth.Day);
            }
        }

        public Person(string newName, string newSurname, DateTime newDateOfBirth)
        {
            name = newName;
            surname = newSurname;
            dateOfBirth = newDateOfBirth;
        }

        public Person()
        {
            name = null;
            surname = null;
            dateOfBirth = DateTime.Today;
        }

        public override string ToString()
        {
                return "Person {" +
                    "name = '" + name + '\'' +
                    ", surname = '" + surname + '\'' +
                    ", dateOfBirth = " + dateOfBirth +
                    '}';
        }

        public virtual string ToShortString()
        {
                return "Person {" +
                    "name = '" + name + '\'' +
                    ", surname = '" + surname + '\'' +
                    '}';
        }

        protected bool Equals(Person other)
        {
            return string.Equals(name, other.name) && string.Equals(surname, other.surname) && dateOfBirth.Equals(other.dateOfBirth);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Person)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (name != null ? name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (surname != null ? surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ dateOfBirth.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Person left, Person right)
        {
            return ReferenceEquals(left, right);
        }

        public static bool operator !=(Person left, Person right)
        {
            return !ReferenceEquals(left, right);
        }

        public virtual object DeepCopy()
        {
            return MemberwiseClone();
        }
    }
}
