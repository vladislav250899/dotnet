﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1Var2
{
    enum Frequency
    {
        Weekly,
        Monthly,
        Yearly
    }

    class Magazine : Edition, IRateAndCopy
    {
        private Frequency periodicity;
        private ArrayList articles = new ArrayList();
        private ArrayList editors = new ArrayList();

        public string GetSetName
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }   

        public Frequency GetSetPeriodicity
        {
            get
            {
                return periodicity;
            }

            set
            {
                periodicity = value;
            }
        }      

        public DateTime GetSetDateOfRelease
        {
            get
            {
                return dateOfRelease;
            }

            set
            {
                dateOfRelease = value;
            }
        }       

        public int GetSetCirculation
        {
            get
            {
                return circulation;
            }

            set
            {
                circulation = value;
            }
        }

        public ArrayList GetSetArticles
        {
            get
            {
                return articles;
            }

            set
            {
                articles = value;
            }
        }

        public ArrayList GetSetEditors
        {
            get
            {
                return editors;
            }

            set
            {
                editors = value;
            }
        }

        public Magazine(string newName, Frequency newPeriodicity, DateTime newDateOfRelease, int newCirculation)
        {
            name = newName;
            periodicity = newPeriodicity;
            dateOfRelease = newDateOfRelease;
            circulation = newCirculation;
        }

        public Magazine()
        {
            name = "";
            periodicity = Frequency.Monthly;
            dateOfRelease = DateTime.Today;
            circulation = 0;
        }

        public double GetAverageRating
        {
            get
            {
                double sum = 0;
                foreach(Article article in articles)
                {
                    sum += article.rating;
                }
                return sum / articles.Count;
            }
        }

        public double Rating => GetAverageRating;

        public Edition EditionBase
        {
            get { return new Edition(name, dateOfRelease, circulation); }
            set
            {
                name = value.GetSetName;
                dateOfRelease = value.GetSetDateOfRelease;
                circulation = value.GetSetCirculation;
            }
        }

        public bool this[Frequency index]
        {
            get
            {
                return periodicity == index;
            }
        }

        public void AddArticles(params Article[] newArticles)
        {
            if (articles != null)
            {
                articles.AddRange(newArticles);
            }
        }

        public void AddEditors(params Person[] newEditors)
        {
            if (editors != null)
            {
                editors.AddRange(newEditors);
            }
        }


        public override string ToString()
        {
            string str1="", str2="";
            int i = 0;
            foreach (Article article in articles)
            {
                str1 += "\n" + (i + 1) + ": " + article + ";";
                i++;
            }
            i = 0;
            foreach (Person editor in editors)
            {
                str2 += "\n" + (i + 1) + ": " + editor + ";";
                i++;
            }
            return "Magazine {" +
                "name = '" + name + '\'' +
                ", periodicity = '" + periodicity + '\'' +
                ", dateOfRelease = " + dateOfRelease +
                ", circulation = " + circulation +
                ", articles = " + str1 +
                ", editors = " + str2 +
                '}';
        }

        public virtual string ToShortString()
        {
            return "Magazine {" +
                "name = '" + name + '\'' +
                ", periodicity = '" + periodicity + '\'' +
                ", dateOfRelease = " + dateOfRelease +
                ", circulation = " + circulation +
                ", averageRating = " + GetAverageRating +
                '}';
        }

        public override object DeepCopy()
        {
            Magazine other = (Magazine)MemberwiseClone();
            other.editors = new ArrayList();
            other.articles = new ArrayList();
            foreach (Person editor in editors)
                other.editors.Add(editor.DeepCopy());
            foreach (Article article in articles)
                other.articles.Add(article.DeepCopy());
            return other;
        }



        public IEnumerable GetArticlesWithRatingMoreThan(double lowValue)
        {
            foreach (Article article in articles)
            {
                if (article.rating > lowValue)
                {
                    yield return article;
                }
            }
        }

        public IEnumerable GetArticlesWithSubstring(string text)
        {
            foreach (Article article in articles)
            {
                if (article.name.Contains(text))
                {
                    yield return article;
                }
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Magazine magazine &&
                   base.Equals(obj) &&
                   periodicity == magazine.periodicity &&
                   EqualityComparer<ArrayList>.Default.Equals(articles, magazine.articles) &&
                   EqualityComparer<ArrayList>.Default.Equals(editors, magazine.editors) &&
                   GetSetName == magazine.GetSetName &&
                   GetSetPeriodicity == magazine.GetSetPeriodicity &&
                   GetSetDateOfRelease == magazine.GetSetDateOfRelease &&
                   GetSetCirculation == magazine.GetSetCirculation &&
                   EqualityComparer<ArrayList>.Default.Equals(GetSetArticles, magazine.GetSetArticles) &&
                   EqualityComparer<ArrayList>.Default.Equals(GetSetEditors, magazine.GetSetEditors) &&
                   GetAverageRating == magazine.GetAverageRating &&
                   Rating == magazine.Rating &&
                   EqualityComparer<Edition>.Default.Equals(EditionBase, magazine.EditionBase);
        }

        public override int GetHashCode()
        {
            var hashCode = 1965486854;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + periodicity.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<ArrayList>.Default.GetHashCode(articles);
            hashCode = hashCode * -1521134295 + EqualityComparer<ArrayList>.Default.GetHashCode(editors);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(GetSetName);
            hashCode = hashCode * -1521134295 + GetSetPeriodicity.GetHashCode();
            hashCode = hashCode * -1521134295 + GetSetDateOfRelease.GetHashCode();
            hashCode = hashCode * -1521134295 + GetSetCirculation.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<ArrayList>.Default.GetHashCode(GetSetArticles);
            hashCode = hashCode * -1521134295 + EqualityComparer<ArrayList>.Default.GetHashCode(GetSetEditors);
            hashCode = hashCode * -1521134295 + GetAverageRating.GetHashCode();
            hashCode = hashCode * -1521134295 + Rating.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Edition>.Default.GetHashCode(EditionBase);
            return hashCode;
        }

        public static bool operator ==(Magazine left, Magazine right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Magazine left, Magazine right)
        {
            return !Equals(left, right);
        }
    }
}
