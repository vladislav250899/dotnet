﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace lab1Var2
{
    class Program
    {
        static void Main(string[] args)
        {
            Person Vasia = new Person("Vasia", "Plantus", new DateTime(1990, 9, 16));
            Person Petia = new Person("Petia", "Rostotskyi", new DateTime(1999, 4, 24));
            Article A = new Article(Vasia, "Job", 45), B = new Article(Vasia, "Lob", 15), C = new Article(Vasia, "Rob", 5);

            Edition first = new Edition();
            Edition second = new Edition();

            Console.WriteLine("First task equals : ");
            Console.WriteLine(" == : " + (first == second));
            Console.WriteLine(" Equals : " + first.Equals(second));
            Console.WriteLine(" Hash : " + first.GetHashCode() + " " + second.GetHashCode());

            Console.WriteLine("\nSecond task exception : ");
            try
            {
                first.GetSetCirculation = -1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //throw;
            }

            Console.WriteLine("\nThird task add Articles and Editors : ");
            Magazine magazine = new Magazine();
            Console.WriteLine("articles: ", magazine.GetSetArticles);
            magazine.AddArticles(A, B);
            Console.WriteLine("articles: ", magazine.GetSetArticles);
            magazine.AddEditors(Vasia,Petia);
            Console.WriteLine(magazine);

            Console.WriteLine("\nFourth task show Edition from Magazine : ");
            Console.WriteLine(magazine.EditionBase);

            Console.WriteLine("\nFifth task DeepCopy : ");
            Magazine magazineCopy = (Magazine)magazine.DeepCopy();
            magazine.GetSetName = "Original";
            Console.WriteLine(magazine.GetSetName + " != " + magazineCopy.GetSetName);

            double moreThan = 10;
            Console.WriteLine("\nSixth task foreach ArticleRage more than " + moreThan + " : ");
            ((Article)magazine.GetSetArticles[0]).Rating = 12;
            foreach (var article in magazine.GetArticlesWithRatingMoreThan(moreThan))
            {
                Console.WriteLine(article);
            }

            string searchText = "ob";
            Console.WriteLine("\nSeventh task foreach ArticleName with " + searchText + " : ");
            foreach (var article in magazine.GetArticlesWithSubstring(searchText))
            {
                Console.WriteLine(article);
            }

            Console.ReadKey();
        }
    }
}
