﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1Var2
{
    class Person
    {
        private string name;
        private string surname;
        private DateTime dateOfBirth;

        public string GetSetName
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string GetSetSurname
        {
            get
            {
                return surname;
            }

            set
            {
                surname = value;
            }
        }

        public DateTime GetSetDateOfBirth
        {
            get
            {
                return dateOfBirth;
            }

            set
            {
                dateOfBirth = value;
            }
        }

        public int YearOfBirth
        {
            get
            {
                return dateOfBirth.Year;
            }

            set
            {
                dateOfBirth = new DateTime(value, dateOfBirth.Month, dateOfBirth.Day);
            }
        }

        public Person(string newName, string newSurname, DateTime newDateOfBirth)
        {
            name = newName;
            surname = newSurname;
            dateOfBirth = newDateOfBirth;
        }

        public Person()
        {
            name = "";
            surname = "";
            dateOfBirth = DateTime.Today;
        }

        public override string ToString()
        {
                return "Person {" +
                    "name = '" + name + '\'' +
                    ", surname = '" + surname + '\'' +
                    ", dateOfBirth = " + dateOfBirth +
                    '}';
        }

        public virtual string ToShortString()
        {
                return "Person {" +
                    "name = '" + name + '\'' +
                    ", surname = '" + surname + '\'' +
                    '}';
        }

        public override bool Equals(object obj)
        {
            return obj is Person person &&
                   name == person.name &&
                   surname == person.surname &&
                   dateOfBirth == person.dateOfBirth &&
                   GetSetName == person.GetSetName &&
                   GetSetSurname == person.GetSetSurname &&
                   GetSetDateOfBirth == person.GetSetDateOfBirth &&
                   YearOfBirth == person.YearOfBirth;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (name != null ? name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (surname != null ? surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ dateOfBirth.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Person left, Person right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Person left, Person right)
        {
            return !Equals(left, right);
        }

        public virtual object DeepCopy()
        {
            return MemberwiseClone();
        }


    }
}
