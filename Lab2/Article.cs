﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1Var2
{
    class Article:IRateAndCopy
    {
        public Person author;
        public string name;
        public double rating;
        public double Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
            }
        }
        public Article(Person newAuthor, string newName, double newRating)
        {
            author = newAuthor;
            name = newName;
            rating = newRating;
        }

        public Article()
        {
            author = new Person();
            name = "";
            rating = 0;
        }

        public override string ToString()
        {
            return "Article {" +
                "author = " + author +
                ", name = '" + name + '\'' +
                ", rating = " + rating +
                '}';
        }

        public virtual object DeepCopy()
        {
            return MemberwiseClone();
        }

        public override bool Equals(object obj)
        {
            return obj is Article article &&
                   EqualityComparer<Person>.Default.Equals(author, article.author) &&
                   name == article.name &&
                   rating == article.rating &&
                   Rating == article.Rating;
        }

        public override int GetHashCode()
        {
            var hashCode = -1541411924;
            hashCode = hashCode * -1521134295 + EqualityComparer<Person>.Default.GetHashCode(author);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + rating.GetHashCode();
            hashCode = hashCode * -1521134295 + Rating.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Article left, Article right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Article left, Article right)
        {
            return !Equals(left, right);
        }
    }
}
