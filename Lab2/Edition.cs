﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1Var2
{
    class Edition
    {
        protected string name;
        protected DateTime dateOfRelease;
        protected int circulation;

        public string GetSetName
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public DateTime GetSetDateOfRelease
        {
            get
            {
                return dateOfRelease;
            }

            set
            {
                dateOfRelease = value;
            }
        }

        public int GetSetCirculation
        {
            get
            {
                return circulation;
            }

            set
            {
                if (value < 0)
                {
                    throw new Exception("Circulation must be positive integer number!");
                }

                circulation = value;
            }
        }

        public Edition(string newName, DateTime newDateOfRelease, int newCirculation)
        {
            name = newName;
            dateOfRelease = newDateOfRelease;
            circulation = newCirculation;
        }

        public Edition()
        {
            name = "";
            circulation = 0;
            dateOfRelease = DateTime.Today;
        }

        public virtual object DeepCopy()
        {
            return MemberwiseClone();
        }

        public override string ToString()
        {
            return "Edition {" +
            "name = '" + name + '\'' +
            ", dateOfRelease = " + dateOfRelease +
            ", circulation = " + circulation +
            '}';
        }





        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (name != null ? name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ dateOfRelease.GetHashCode();
                hashCode = (hashCode * 397) ^ circulation;
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Edition edition &&
                   name == edition.name &&
                   dateOfRelease == edition.dateOfRelease &&
                   circulation == edition.circulation &&
                   GetSetName == edition.GetSetName &&
                   GetSetDateOfRelease == edition.GetSetDateOfRelease &&
                   GetSetCirculation == edition.GetSetCirculation;
        }

        public static bool operator ==(Edition left, Edition right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Edition left, Edition right)
        {
            return !Equals(left, right);
        }
    }
}
